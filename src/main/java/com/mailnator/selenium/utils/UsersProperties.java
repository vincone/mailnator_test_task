package com.mailnator.selenium.utils;

import java.util.Properties;

public class UsersProperties {

	private static String getProperty(String propertyName) {

		Properties properties = new Properties();
		try {
			properties.load(UsersProperties.class.getClassLoader().getResourceAsStream("users.properties"));
		}

		catch (Exception e) {
		}

		return properties.getProperty(propertyName);
	}

	public static String getUser16Email() {

		return UsersProperties.getProperty("user16.email");
	}

    public static String getUser18Email() {

        return UsersProperties.getProperty("user18.email");
    }

    public static String getUser19Email() {

        return UsersProperties.getProperty("user19.email");
    }

    public static String getUsersPassword() {

        return UsersProperties.getProperty("users.password");
    }
    
}
