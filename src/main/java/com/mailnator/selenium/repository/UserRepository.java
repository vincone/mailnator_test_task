package com.mailnator.selenium.repository;

import java.util.ArrayList;

import com.mailnator.selenium.entities.User;
import com.mailnator.selenium.utils.UsersProperties;

public class UserRepository {

    private static final String VALID_PASSWORD = UsersProperties.getUsersPassword();

    private static final User USER_16 = new User.Builder().setEmail(UsersProperties.getUser16Email()).setPassword(VALID_PASSWORD).setSavedEmails(true).build();
    private static final User USER_18 = new User.Builder().setEmail(UsersProperties.getUser18Email()).setPassword(VALID_PASSWORD).setSavedEmails(false).build();
    private static final User USER_19 = new User.Builder().setEmail(UsersProperties.getUser19Email()).setPassword(VALID_PASSWORD).setSavedEmails(true).build();

	
	public static ArrayList<User> getUsers(){
		
		ArrayList<User> testUsers = new ArrayList<User>();
		testUsers.add(UserRepository.USER_16);
		testUsers.add(UserRepository.USER_18);
		testUsers.add(UserRepository.USER_19);
		return testUsers;
	}
	
}
