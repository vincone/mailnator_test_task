package com.mailnator.selenium.repository;

public class URLRepository {

	private static final String MAILNATOR_URL = "https://www.mailinator.com/";

	public static final String HOME_PAGE_URL = MAILNATOR_URL;
	public static final String LOGIN_PAGE_URL = HOME_PAGE_URL + "manyauth/login.jsp";
	public static final String LOG_OUT_URL = HOME_PAGE_URL + "manyauth/logout.jsp";
	
}
