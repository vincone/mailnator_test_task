package com.mailnator.selenium.pages;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

import com.mailnator.selenium.entities.CheckResult;
import com.mailnator.selenium.entities.User;
import com.mailnator.selenium.utils.WebDriverUtils;

public class HomePage {

	public HomePage() {
		
		PageFactory.initElements(WebDriverUtils.getDriver(), this);
	}

	/**
	 * Navigation bar controls
	 */
	@FindBy(linkText = "Email")
	public static WebElement emailTab;
	@FindBy(linkText = "Pricing")
	public static WebElement pricingTab;
	@FindBy(linkText = "FAQ")
	public static WebElement FAQTab;
	@FindBy(linkText = "Docs")
	public static WebElement DocsTab;
	@FindBy(linkText = "login")
	public static WebElement loginTab;
	@FindBy(linkText = "signup")
	public static WebElement signupTab;
	@FindBy(css = ".navbar-left a b")
	public static WebElement userNameTab;

	/**
	 * Navigate to login page
	 */
	public LoginPage goToLogin() {
		
		loginTab.click();
		return new LoginPage();
	}

	/**
	 * Navigate to email page
	 */
	public EmailPage goToEmail() {
		
		emailTab.click();
		return new EmailPage();
	}

	/**
	 * Function is used to check user login successful
	 *
	 * @param user
	 */
	public CheckResult checkUserLoginSuccessful(User user) {
		
		WebDriverUtils.waitElementToBeClickable(userNameTab);
		return new CheckResult(userNameTab.getText().equals(user.getEmail()), "User email tab exist");
	}
}
