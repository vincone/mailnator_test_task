package com.mailnator.selenium.utils;

import java.io.File;
import java.io.IOException;
import java.util.concurrent.TimeUnit;

import org.apache.commons.io.FileUtils;
import org.openqa.selenium.OutputType;
import org.openqa.selenium.TakesScreenshot;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import com.mailnator.selenium.repository.URLRepository;

public class WebDriverUtils {

	private static WebDriver driver = new FirefoxDriver();

	public static WebDriver initDriver() {

		driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
		driver.manage().timeouts().pageLoadTimeout(10, TimeUnit.SECONDS);
		driver.manage().window().maximize();
		
		return driver;
	}

	public static void navigate(String Url) {
		
		driver.get(Url);
	}
	
	public static void logOut() {
		
		driver.get(URLRepository.LOG_OUT_URL);
	}

	public static void stop() {
		
		driver.quit();
		driver = null;
	}
	
    public static synchronized WebDriver getDriver() {

        if (driver == null) {
            driver = initDriver();
        }
        return driver;
    }
    
    public static void getScreenShot(String name) {

        String strDirectory = "C:/screenshots/";
        File file = new File(strDirectory);
        if (!(file.exists())) {
            file.mkdir();
        }

        File screenshot = ((TakesScreenshot)driver).getScreenshotAs(OutputType.FILE);
        String identifierForFile = CommonActionHelpers.getCurrentDate("yyyyMMddHHmmss");
        String path = strDirectory + identifierForFile + name + ".jpg";

        try {
            FileUtils.copyFile(screenshot, new File(path));
            FileUtils.forceDelete(screenshot);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
    
    public static void waitElementToBeClickable(WebElement element) {
    	
		WebDriverWait wait = new WebDriverWait(driver, 15);
		wait.until(ExpectedConditions.elementToBeClickable(element));
    }
    
}
