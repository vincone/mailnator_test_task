package com.mailnator.selenium.utils;

import org.apache.log4j.LogManager;
import org.apache.log4j.Logger;
import org.junit.AfterClass;
import org.junit.Assert;
import org.junit.BeforeClass;
import org.junit.Rule;
import org.junit.rules.TestWatcher;
import org.junit.runner.Description;

import com.mailnator.selenium.entities.CheckResult;
import com.mailnator.selenium.repository.URLRepository;

public class TestBase {

	private static Logger logger = LogManager.getLogger(TestBase.class);

	@Rule
	public TestWatcher watchman = new TestWatcher() {

		@Override
		protected void failed(Throwable e, Description description) {
			
			logger.error(description.getClassName() + ": " + description.getMethodName() + ": failed", e);
			WebDriverUtils.getScreenShot(description.getClassName() + "_" + description.getMethodName());
		}

		@Override
		protected void succeeded(Description description) {
			
			logger.info(description.getClassName() + ": " + description.getMethodName() + ": passed");
		}
	};

	@BeforeClass
	public static void init() {

		WebDriverUtils.navigate(URLRepository.HOME_PAGE_URL);
	}

	@AfterClass
	public static void stopDriver() {
		
		WebDriverUtils.stop();
	}

	public static void assertCheckPassed(CheckResult checkResult) {

		Assert.assertTrue(checkResult.getDescription(), checkResult.getPassed());
	}
}