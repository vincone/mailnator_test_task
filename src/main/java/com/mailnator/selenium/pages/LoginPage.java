package com.mailnator.selenium.pages;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

import com.mailnator.selenium.entities.User;
import com.mailnator.selenium.utils.WebDriverUtils;

public class LoginPage {

	public LoginPage() {
		
		PageFactory.initElements(WebDriverUtils.getDriver(), this);
	}

	/**
	 * Login controls
	 */
	@FindBy(id = "many_login_email")
	public static WebElement loginEmail;
	@FindBy(id = "many_login_password")
	public static WebElement password;
	@FindBy(xpath = "//button[@onclick='many_login();' and contains(., 'Login')]")
	public static WebElement loginButton;

	/**
	 * Function is used to login user
	 *
	 * @param user
	 */
	public HomePage login(User user) {
		
		loginEmail.sendKeys(user.getEmail());
		password.sendKeys(user.getPassword());
		loginButton.click();
		WebDriverUtils.waitElementToBeClickable(HomePage.emailTab);
		return new HomePage();
	}
}
