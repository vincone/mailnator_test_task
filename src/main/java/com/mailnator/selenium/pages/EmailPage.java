package com.mailnator.selenium.pages;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

import com.mailnator.selenium.entities.CheckResult;
import com.mailnator.selenium.utils.WebDriverUtils;

public class EmailPage {

	public EmailPage() {
		
		PageFactory.initElements(WebDriverUtils.getDriver(), this);
	}
	
	@FindBy(xpath = "//*[@id='bigrow_personal']/span")
	public WebElement savedEmailCount;
	
	/**
	 * Function is used to check existing saved emails
	 *
	 * @param value
	 */
    public CheckResult checkSavedEmailsEmpty(Boolean value) {
    	
		WebDriverUtils.waitElementToBeClickable(savedEmailCount);
        return new CheckResult((Integer.parseInt(savedEmailCount.getText()) == 0) == value, "Emails more then one");
    }
}
