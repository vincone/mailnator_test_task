Test Case ID: 1
Test Scenario: Check User Login and Saved emails
Test Steps:
	1. Go to site
		https://www.mailinator.com/
	2. Go to "EMAIL" page
	3. Enter @UserEmail
	4. Enter @Password
	5. Click login.
	6. Go to "EMAIL" page
	7. Check @SavedEmails value
Expected Results:
	1. Site opened
	2. Email page opened
	5. User should Login into application and Start page should be opened
	6. Email page opened
Test Date:
	1. UserEmail = testtask1116@mailinator.com
	   Password = ZXCasdqwe123
	   SavedEmails = true

	2. UserEmail = testtask1118@mailinator.com
	   Password = ZXCasdqwe123
	   SavedEmails = false

	3. UserEmail = testtask1119@mailinator.com
	   Password = ZXCasdqwe123
	   SavedEmails = true