package com.mailnator.selenium.utils;

import java.text.SimpleDateFormat;
import java.util.Date;

public class CommonActionHelpers {

    public static String getCurrentDate(String simpleDateFormat) {

        Date date = new Date();
        String currentDate = new SimpleDateFormat(simpleDateFormat).format(date).toString();
        return currentDate;
    }
}
