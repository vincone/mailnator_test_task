package com.mailnator.selenium;

import java.util.ArrayList;
import java.util.Collection;

import org.junit.After;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.Parameterized;

import com.mailnator.selenium.entities.User;
import com.mailnator.selenium.pages.EmailPage;
import com.mailnator.selenium.pages.HomePage;
import com.mailnator.selenium.pages.LoginPage;
import com.mailnator.selenium.repository.UserRepository;
import com.mailnator.selenium.utils.TestBase;
import com.mailnator.selenium.utils.WebDriverUtils;

@RunWith(Parameterized.class)
public class MailnatorTest extends TestBase {
	
	public static final HomePage HOME_PAGE = new HomePage();
	public static final LoginPage LOGIN_PAGE = new LoginPage();
	public static final EmailPage EMAIL_PAGE = new EmailPage();

    private static User testUser;
    private static ArrayList<Object[]> users = new ArrayList<Object[]>();
	
	@After
	public void after() {
		
		WebDriverUtils.logOut();
	}

    static {
    	
        for (User usr : UserRepository.getUsers()) {
        	users.add(new Object[]{usr});
        }
    }

    @Parameterized.Parameters(name="{0}")
    public static Collection<Object[]> users() {
    	
        return users;
    }

    public MailnatorTest(User user){
    	
    	testUser = user;
    }

	@Test
	public void testLogin() {
		
		HOME_PAGE.goToLogin().login(testUser);
		assertCheckPassed(HOME_PAGE.checkUserLoginSuccessful(testUser));
	}

	@Test
	public void testEmails() {
		
		HOME_PAGE.goToLogin().login(testUser).goToEmail();
		assertCheckPassed(EMAIL_PAGE.checkSavedEmailsEmpty(testUser.getStatus()));
	}
}
