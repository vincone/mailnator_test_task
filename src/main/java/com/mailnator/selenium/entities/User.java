package com.mailnator.selenium.entities;

public class User {
	@Override
	public String toString() {
		return "User [login=" + email + "]";
	}

	private String email;
	private String password;
	private Boolean savedEmails;

	public static class Builder {

		private String email = null;
		private String password = null;
		private Boolean savedEmails = null;

		public Builder setEmail(String val) {

			this.email = val;
			return this;
		}

		public Builder setPassword(String val) {

			this.password = val;
			return this;
		}

		public Builder setSavedEmails(Boolean val) {

			this.savedEmails = val;
			return this;
		}

		public User build() {

			return new User(this);
		}
	}

	private User(Builder builder) {

	        this.email = builder.email;
	        this.password = builder.password;
	        this.savedEmails = builder.savedEmails;
	    }

	public String getEmail() {

		return email;
	}

	public String getPassword() {

		return password;
	}

	public Boolean getStatus() {

		return savedEmails;
	}
}
