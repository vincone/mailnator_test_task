package com.mailnator.selenium.entities;

public class CheckResult {

    private boolean passed;

    private String description;

    public CheckResult(boolean passed, String description) {

        this.passed = passed;
        this.description = description;
    }

    public boolean getPassed() {

        return passed;
    }

    public String getDescription() {

        return description;
    }

}
